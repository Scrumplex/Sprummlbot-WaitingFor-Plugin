# Sprummlbot-WaitingFor-Plugin
A plugin for the [Sprummlbot](https://sprum.ml) by the official developer.

---
This plugin adds a new command to the Sprummlbot named `!waitingfor`. With that plugin you can have a reminder when a client joins.

[Forum Post (Download and more)](https://forum.sprum.ml/thread.php?pid=90)

##Installation
1. Download or compile the jar file
2. Upload it to your server
3. Put the jar into your `plugins` directory.
4. Restart the Sprummlbot

##Compiling
It is a Java project *made in IntelliJ IDEA*, so just import it into your ide and compile it.

##License
[Apache License 2.0](LICENSE)

##Credits
Project maintained by [Scrumplex](https://scrumplex.ovh).